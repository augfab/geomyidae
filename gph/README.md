# gph format

## vim
* Here you find syntax highlighting for gph files for vim.
	* Thanks dive on #gopherproject for contributing this!

### Installation

	cp vim/ftdetect/gph.vim ~/.vim/ftdetect
	cp vim/syntax/gph.vim ~/.vim/syntax

## emacs

Please read this on how emacs finds its major mode files:

	https://www.gnu.org/software/emacs/manual/html_node/elisp/Auto-Major-Mode.html

